import argparse

parser = argparse.ArgumentParser(description='Parsing output of rsync')
parser.add_argument('stats', type=str, help='rsync stats output')

args = parser.parse_args()

all_words = args.stats.split()


def find_sequence(phrase: str):
    words = phrase.split()
    for i in range(len(all_words)):
        if all_words[i] == words[0]:
            found = True
            for j in range(len(words)):
                if i + j > len(all_words):
                    found = False
                    break;
                if not words[j] == all_words[i + j]:
                    found = False
                    break;
            if found:
                return all_words[i + len(words)]
    return 0


data_size = float(find_sequence("Total transferred file size:").replace(',','.'))
num_files = int(find_sequence("Number of regular files transferred:"))


print("Data size {}, Number of files {}".format(data_size, num_files))
